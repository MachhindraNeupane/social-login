package com.example.machhindra.macnewintegration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    LoginButton loginButton;
    TextView textView;
    CallbackManager callbackManager;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            FacebookSdk.sdkInitialize(getApplicationContext());
            AppEventsLogger.activateApp(this);
            setContentView(R.layout.activity_main);
            loginButton = (LoginButton) findViewById(R.id.login_facebook);
            textView = (TextView) findViewById(R.id.textview);
            callbackManager = CallbackManager.Factory.create();
            imageView = (ImageView) findViewById(R.id.imageview);
            loginButton.setReadPermissions("email");
            loginButton.setReadPermissions("user_birthday");
            loginButton.setReadPermissions("user_location");

            loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    final GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            JSONObject jsonObject = response.getJSONObject();
                            JSONArray jsonArray = response.getJSONArray();
                            Log.e("JsonObject", response.getJSONObject().toString());
                            try {
                                Log.e("first_name", jsonObject.getString("first_name"));
                                Log.e("last_name", jsonObject.getString("last_name"));
                                Log.e("email", jsonObject.getString("email"));
                                Log.e("Gender", jsonObject.getString("gender"));
                                Log.e("location", jsonObject.getJSONObject("location").getString("name"));
                                Log.e("Birthday", jsonObject.getString("birthday"));
                            } catch (JSONException e) {
                                Log.e("Exception Json", e.getMessage());
                            }
                            try {
                                URL profile_pic = new URL("https://graph.facebook.com/" + object.optString("id") + "/picture?width=200&height=150");
                                Picasso.with(getApplicationContext()).load(String.valueOf(profile_pic)).into(imageView);
                            } catch (MalformedURLException e) {
                                Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id, first_name, last_name,email,gender,birthday,location"); // Parámetros que pedimos a facebook
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                @Override
                public void onCancel() {
                    textView.setText("Log in Cancled");
                }

                @Override
                public void onError(FacebookException error) {
                    textView.setText("Login Error");
                }
            });
        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}

  /*public void generateHashValue() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.example.machhindra.macnewintegration",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("First Exception", e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Log.e("Second Exception", e.getMessage());
        }
    }
*/